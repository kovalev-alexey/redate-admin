import router from '@/router'
import { useUserStore } from '@/stores/user'
import { UserApi } from '@/api/user'

class Service {
  async saveTokens(data) {
    const userStore = useUserStore()

    const date = Date.now()

    const tokens = {
      access: {
        token: data.access.token,
        expiredAt: date + data.access.liveTime,
      },
      refresh: {
        token: data.refresh.token,
        expiredAt: date + data.refresh.liveTime,
      },
    }

    userStore.tokens.data = tokens

    localStorage.setItem('tokens', JSON.stringify(tokens))
  }

  logout() {
    const userStore = useUserStore()

    userStore.tokens.data = null

    localStorage.removeItem('tokens')

    router.push({ name: 'Auth' })
  }

  async userInfo() {
    const userStore = useUserStore()

    userStore.userInfo.isLoading = true

    const { data, error } = await UserApi.userInfo()

    if (error.status) {
      userStore.userInfo.error = error

      userStore.userInfo.isLoading = false

      return
    }

    userStore.userInfo.data = data

    userStore.userInfo.isLoading = false
  }
}

export const UserService = new Service()
