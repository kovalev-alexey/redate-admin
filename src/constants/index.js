// Роли администраторов и доступные им страницы, ключ объекта это номер роли, значение это имена страниц из routes.js
// 5 - Главный администратор, он же Super user
// 6 - Администратор системы
// 7 - Модератор
// 8 - Начинающий модератор
// 13 - Заблокированный администратор

const AVAILABLE_PAGES_BY_ROLE = {
  5: [
    'Home',
    'Complaints',
    'Mailings',
    'Verification',
    'Moderation',
    'SupportChat',
    'UsersChat',
    'Blocking',
    'SearchUser',
    'Search',
    'ChatsPage',
    'SingleChat',
    'Metrics',
    'AdminControl',
    'ServerError',
    'NotFound',
    'AccessDenied',
    'Auth',
  ],
  6: [
    'Home',
    'Complaints',
    'Mailings',
    'Verification',
    'Moderation',
    'SupportChat',
    'Blocking',
    'SearchUser',
    'Search',
    'ServerError',
    'NotFound',
    'AccessDenied',
    'Auth',
  ],
  7: [
    'Home',
    'Complaints',
    'Mailings',
    'Verification',
    'Moderation',
    'SupportChat',
    'Blocking',
    'SearchUser',
    'Search',
    'ServerError',
    'NotFound',
    'AccessDenied',
    'Auth',
  ],
  8: [
    'Home',
    'Complaints',
    'Mailings',
    'Verification',
    'Moderation',
    'Blocking',
    'SearchUser',
    'Search',
    'ServerError',
    'NotFound',
    'AccessDenied',
    'Auth',
  ],
  13: ['ServerError', 'NotFound', 'AccessDenied', 'Auth'],
}

export { AVAILABLE_PAGES_BY_ROLE }
