import { defineStore } from 'pinia'

export const useUserStore = defineStore('user', {
  state: () => ({
    tokens: {
      data: JSON.parse(localStorage.getItem('tokens')) || null,
    },
    userInfo: {
      data: null,
      error: null,
      isLoading: false,
    },
  }),
})
