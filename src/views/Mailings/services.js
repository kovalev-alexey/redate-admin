import router from '@/router'
import { AppService } from '@/views/App/services'
import { MailingsApi } from './api'
import { useMailingsStore } from './store/mailings'

class Service {
  async onModerationMailings(page = 0, count = 1000) {
    const mailingsStore = useMailingsStore()

    mailingsStore.onModerationMailings.isLoading = true

    const status = 1

    const pagination = {
      page,
      count,
    }

    const { data, error } = await MailingsApi.mailings(status, pagination)

    if (error.status) {
      mailingsStore.onModerationMailings.error = error
      mailingsStore.onModerationMailings.isLoading = false
      return
    }

    const onModerationMailingsCount = data.wait === 0 ? null : data.wait

    mailingsStore.tabs = mailingsStore.tabs.map((tab) =>
      tab.componentName === 'MailingsOnModeration'
        ? { ...tab, count: onModerationMailingsCount }
        : tab
    )

    mailingsStore.onModerationMailings.data = data

    mailingsStore.onModerationMailings.isLoading = false
  }

  async activeMailings(page = 0, count = 1000) {
    const mailingsStore = useMailingsStore()

    mailingsStore.activeMailings.isLoading = true

    const status = 2

    const pagination = {
      page,
      count,
    }

    const { data, error } = await MailingsApi.mailings(status, pagination)

    if (error.status) {
      mailingsStore.activeMailings.error = error
      mailingsStore.activeMailings.isLoading = false
      return
    }

    const activeMailingsCount = data.active === 0 ? null : data.active

    mailingsStore.tabs = mailingsStore.tabs.map((tab) =>
      tab.componentName === 'MailingsActive'
        ? { ...tab, count: activeMailingsCount }
        : tab
    )

    mailingsStore.activeMailings.data = data

    mailingsStore.activeMailings.isLoading = false
  }

  async rejectedMailings(page = 0, count = 1000) {
    const mailingsStore = useMailingsStore()

    mailingsStore.rejectedMailings.isLoading = true

    const status = 4

    const pagination = {
      page,
      count,
    }

    const { data, error } = await MailingsApi.mailings(status, pagination)

    if (error.status) {
      mailingsStore.rejectedMailings.error = error

      mailingsStore.rejectedMailings.isLoading = false

      return
    }

    mailingsStore.rejectedMailings.data = data

    mailingsStore.rejectedMailings.isLoading = false
  }

  async startMailing(mailingId) {
    const mailingsStore = useMailingsStore()

    mailingsStore.startMailing.isLoading = true

    const { error } = await MailingsApi.startMailing(mailingId)

    if (error.status) {
      mailingsStore.startMailing.isLoading = false

      router.push({ name: 'ServerError' })

      return
    }

    await AppService.update()

    mailingsStore.startMailing.isLoading = false
  }

  async rejectMailing(mailingId, rejectReason) {
    const mailingsStore = useMailingsStore()

    mailingsStore.rejectMailing.isLoading = true

    const { error } = await MailingsApi.rejectMailing(mailingId, rejectReason)

    if (error.status) {
      mailingsStore.rejectMailing.isLoading = false

      router.push({ name: 'ServerError' })

      return
    }

    await AppService.update()

    mailingsStore.rejectMailing.isLoading = false
  }

  async stopMailing(mailingId) {
    const mailingsStore = useMailingsStore()

    mailingsStore.stopMailing.isLoading = true

    const { error } = await MailingsApi.stopMailing(mailingId)

    if (error.status) {
      mailingsStore.stopMailing.isLoading = false

      router.push({ name: 'ServerError' })

      return
    }

    await AppService.update()

    mailingsStore.stopMailing.isLoading = false
  }
}

export const MailingsService = new Service()
