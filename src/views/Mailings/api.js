import { API } from '@/api/apiService.js'

class ApiService {
  async mailings(status, pagination) {
    try {
      const mailingsRequestData = {
        status,
        page: pagination,
      }

      const mailingsResponse = await API.post(
        '/mailing/list/admin',
        mailingsRequestData
      )

      if (!mailingsResponse.status) {
        throw new Error(mailingsResponse.message)
      }

      return {
        data: mailingsResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async startMailing(mailingId) {
    try {
      const startMailingResponse = await API.get(`/mailing/${mailingId}/start`)

      if (!startMailingResponse.status) {
        throw new Error(startMailingResponse.message)
      }

      return {
        data: startMailingResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async rejectMailing(mailingId, rejectReason) {
    try {
      const rejectMailingRequestData = {
        rejectReason,
      }

      const rejectMailingResponse = await API.post(
        `/mailing/${mailingId}/reject`,
        rejectMailingRequestData
      )

      if (!rejectMailingResponse.status) {
        throw new Error(rejectMailingResponse.message)
      }

      return {
        data: rejectMailingResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async stopMailing(mailingId) {
    try {
      const stopMailingResponse = await API.get(`/mailing/${mailingId}/stop`)

      if (!stopMailingResponse.status) {
        throw new Error(stopMailingResponse.message)
      }

      return {
        data: stopMailingResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
}

export const MailingsApi = new ApiService()
