import { defineStore } from 'pinia'

export const useMailingsStore = defineStore('mailings', {
  state: () => ({
    currentTab: {},
    tabs: [
      {
        id: 1,
        title: 'Рассылки на модерации',
        count: null,
        componentName: 'MailingsOnModeration',
        mod: 'on-moderation',
      },
      {
        id: 2,
        title: 'Активные',
        count: null,
        componentName: 'MailingsActive',
        mod: 'active',
      },
      {
        id: 3,
        title: 'Отклоненные',
        count: null,
        componentName: 'MailingsRejected',
        mod: 'rejected',
      },
    ],
    onModerationMailings: {
      data: {
        list: [],
        active: null,
        wait: null,
        rejected: null,
      },
      error: null,
      isLoading: false,
    },
    activeMailings: {
      data: {
        list: [],
        active: null,
        wait: null,
        rejected: null,
      },
      error: null,
      isLoading: false,
    },
    rejectedMailings: {
      data: {
        list: [],
        active: null,
        wait: null,
        rejected: null,
      },
      error: null,
      isLoading: false,
    },
    startMailing: {
      isLoading: false,
    },
    rejectMailing: {
      isLoading: false,
    },
    stopMailing: {
      isLoading: false,
    },
  }),

  actions: {
    setCurrentTab(tab) {
      this.currentTab = tab
    },
  },
})
