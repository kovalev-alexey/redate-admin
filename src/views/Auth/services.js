import router from '@/router'
import { AuthApi } from './api'
import { UserService } from '@/services/user'
import { useAuthStore } from './store/auth'
import { encryptPassword } from '@/utils/encrypt'

class Service {
  async auth(login, password) {
    const authStore = useAuthStore()

    authStore.auth.isLoading = true

    const encryptedPassword = encryptPassword(password)

    const { data, error } = await AuthApi.auth(login, encryptedPassword)

    if (error.status) {
      authStore.auth.error = error

      authStore.auth.isLoading = false

      return
    }

    UserService.saveTokens(data)

    authStore.auth.isLoading = false

    router.push({ name: 'Complaints' })
  }

  async updateTokens(refreshToken) {
    const { data, error } = await AuthApi.updateTokens(refreshToken)

    if (error.status) {
      return error
    }

    UserService.saveTokens(data)

    return data
  }
}

export const AuthService = new Service()
