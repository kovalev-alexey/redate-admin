import { API } from '@/api/apiService.js'

class ApiService {
  async auth(login, encryptedPassword) {
    try {
      const authRequestData = {
        login,
        password: encryptedPassword,
      }

      const authResponse = await API.post(
        '/auth/admin',
        authRequestData,
        null,
        true
      )

      if (!authResponse.status) {
        throw new Error(authResponse.message)
      }

      return { data: authResponse.data, error: { status: false, message: '' } }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async updateTokens(refreshToken) {
    try {
      const updateTokensRequestData = {
        token: refreshToken,
      }

      const updateTokensResponse = await API.post(
        '/auth/token/update',
        updateTokensRequestData
      )

      if (!updateTokensResponse.status) {
        throw new Error(updateTokensResponse.message)
      }

      return {
        data: updateTokensResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
}

export const AuthApi = new ApiService()
