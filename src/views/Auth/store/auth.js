import { defineStore } from 'pinia'

export const useAuthStore = defineStore('auth', {
  state: () => ({
    auth: {
      error: null,
      isLoading: false,
    },
  }),
})
