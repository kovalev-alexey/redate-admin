import { defineStore } from 'pinia'

export const useComplaintsStore = defineStore('complaints', {
  state: () => ({
    complaints: {
      data: {
        list: [],
        all: null,
      },
      error: null,
      isLoading: false,
    },
    deleteComplaint: {
      isLoading: false,
    },
  }),
})
