import router from '@/router'
import { AppService } from '@/views/App/services'
import { useComplaintsStore } from './store/complaints'
import { ComplaintsApi } from './api'

class Service {
  async complaints(page = 0, count = 1000) {
    const complaintsStore = useComplaintsStore()

    complaintsStore.complaints.isLoading = true

    const pagination = {
      page,
      count,
    }

    const { data, error } = await ComplaintsApi.complaints(pagination)

    if (error.status) {
      complaintsStore.complaints.error = error

      complaintsStore.complaints.isLoading = false

      return
    }

    complaintsStore.complaints.data = data

    complaintsStore.complaints.isLoading = false
  }

  async deleteComplaint(complaintId) {
    const complaintsStore = useComplaintsStore()

    complaintsStore.deleteComplaint.isLoading = true

    const { error } = await ComplaintsApi.deleteComplaint(complaintId)

    if (error.status) {
      router.push({ name: 'ServerError' })

      return
    }

    await AppService.update()

    complaintsStore.deleteComplaint.isLoading = false
  }
}

export const ComplaintsService = new Service()
