import { API } from '@/api/apiService.js'

class ApiService {
  async complaints(pagination) {
    try {
      const complaintsResponse = await API.post('/complaints/list', pagination)

      if (!complaintsResponse.status) {
        throw new Error(complaintsResponse.message)
      }

      return {
        data: complaintsResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async deleteComplaint(complaintId) {
    try {
      const deleteComplaintResponse = await API.get(
        `/complaints/${complaintId}/delete`
      )

      if (!deleteComplaintResponse.status) {
        throw new Error(deleteComplaintResponse.message)
      }

      return {
        data: deleteComplaintResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
}

export const ComplaintsApi = new ApiService()
