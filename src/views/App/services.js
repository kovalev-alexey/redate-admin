import router from '@/router'
import { useAppStore } from './store/app'
import { useUserStore } from '@/stores/user'
import { useSidebarStore } from './store/sidebar'
import { useComplaintsStore } from '@/views/Complaints/store/complaints'
import { useMailingsStore } from '@/views/Mailings/store/mailings'
import { UserService } from '@/services/user'
import { ComplaintsService } from '@/views/Complaints/services'
import { MailingsService } from '@/views/Mailings/services'
import { AVAILABLE_PAGES_BY_ROLE } from '@/constants'

class Service {
  async init() {
    const appStore = useAppStore()
    const userStore = useUserStore()
    const sidebarStore = useSidebarStore()
    const complaintsStore = useComplaintsStore()
    const mailingsStore = useMailingsStore()

    appStore.app.isLoading = true

    await UserService.userInfo()

    const userInfoError = userStore.userInfo.error

    if (userInfoError?.status) {
      router.push({ name: 'ServerError' })

      return
    }

    const userInfo = userStore.userInfo.data

    const pagesAllowedAdmin = AVAILABLE_PAGES_BY_ROLE[userInfo?.role]

    sidebarStore.menu = sidebarStore.menu.map((item) => {
      let allowed = false

      pagesAllowedAdmin.forEach((page) => {
        if (item.name === page) {
          allowed = true
        }
      })

      return allowed ? { ...item, show: true } : item
    })

    await ComplaintsService.complaints()

    const complaintsAll = complaintsStore.complaints.data?.all || null

    sidebarStore.menu = sidebarStore.menu.map((item) =>
      item.name === 'Complaints' ? { ...item, count: complaintsAll } : item
    )

    await MailingsService.onModerationMailings()

    await MailingsService.activeMailings()

    await MailingsService.rejectedMailings()

    const mailingsAll =
      (mailingsStore.onModerationMailings.data.active || 0) +
      (mailingsStore.onModerationMailings.data.wait || 0)

    if (mailingsAll !== 0) {
      sidebarStore.menu = sidebarStore.menu.map((item) =>
        item.name === 'Mailings' ? { ...item, count: mailingsAll } : item
      )
    }

    appStore.app.isLoading = false
  }

  async update() {
    const userStore = useUserStore()
    const sidebarStore = useSidebarStore()
    const complaintsStore = useComplaintsStore()
    const mailingsStore = useMailingsStore()

    await UserService.userInfo()

    const userInfoError = userStore.userInfo.error

    if (userInfoError?.status) {
      router.push({ name: 'ServerError' })

      return
    }

    const userInfo = userStore.userInfo.data

    const pagesAllowedAdmin = AVAILABLE_PAGES_BY_ROLE[userInfo?.role]

    sidebarStore.menu = sidebarStore.menu.map((item) => {
      let allowed = false

      pagesAllowedAdmin.forEach((page) => {
        if (item.name === page) {
          allowed = true
        }
      })

      return allowed ? { ...item, show: true } : item
    })

    await ComplaintsService.complaints()

    const complaintsAll = complaintsStore.complaints.data?.all || null

    sidebarStore.menu = sidebarStore.menu.map((item) =>
      item.name === 'Complaints' ? { ...item, count: complaintsAll } : item
    )

    await MailingsService.onModerationMailings()

    await MailingsService.activeMailings()

    await MailingsService.rejectedMailings()

    const mailingsAll =
      (mailingsStore.onModerationMailings.data.active || 0) +
      (mailingsStore.onModerationMailings.data.wait || 0)

    if (mailingsAll !== 0) {
      sidebarStore.menu = sidebarStore.menu.map((item) =>
        item.name === 'Mailings' ? { ...item, count: mailingsAll } : item
      )
    } else {
      sidebarStore.menu = sidebarStore.menu.map((item) =>
        item.name === 'Mailings' ? { ...item, count: null } : item
      )
    }
  }

  pageAccessController(currentRoute, role) {
    const pagesAllowedAdmin = AVAILABLE_PAGES_BY_ROLE[role]

    const access =
      pagesAllowedAdmin.filter((page) => page === currentRoute.name).length > 0

    if (!access) {
      router.push({ name: 'AccessDenied' })

      return
    }
  }
}

export const AppService = new Service()
