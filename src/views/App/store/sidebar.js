import { defineStore } from 'pinia'
import ComplaintsIcon from '@/assets/icons/complaints.svg'
import MailingsIcon from '@/assets/icons/mailings.svg'
import VerificationIcon from '@/assets/icons/verification.svg'
import ModerationIcon from '@/assets/icons/moderation.svg'
import SupportChatIcon from '@/assets/icons/support-chat.svg'
import BlockingIcon from '@/assets/icons/blocking.svg'
import SearchIcon from '@/assets/icons/search.svg'
import MetricsIcon from '@/assets/icons/metrics.svg'

export const useSidebarStore = defineStore('sidebar', {
  state: () => ({
    menu: [
      {
        title: 'Жалобы',
        url: '/',
        name: 'Complaints',
        icon: ComplaintsIcon,
        count: null,
        show: false,
      },
      {
        title: 'Рассылки',
        url: '/mailings',
        name: 'Mailings',
        icon: MailingsIcon,
        count: null,
        show: false,
      },
      {
        title: 'Верификация',
        url: '/verification',
        name: 'Verification',
        icon: VerificationIcon,
        count: null,
        show: false,
      },
      {
        title: 'Модерация',
        url: '/moderation',
        name: 'Moderation',
        icon: ModerationIcon,
        count: null,
        show: false,
      },
      {
        title: 'Чат поддержки',
        url: '/support-chat',
        name: 'SupportChat',
        icon: SupportChatIcon,
        count: null,
        show: false,
      },
      {
        title: 'Блокировки',
        url: '/blocking',
        name: 'Blocking',
        icon: BlockingIcon,
        count: null,
        show: false,
      },
      {
        title: 'Поиск по ID',
        url: '/search',
        name: 'Search',
        icon: SearchIcon,
        count: null,
        show: false,
      },
      {
        title: 'Метрика сайта',
        url: '/metrics',
        name: 'Metrics',
        icon: MetricsIcon,
        count: null,
        show: false,
      },
      {
        title: 'Контроль админов',
        url: '/admin-control',
        name: 'AdminControl',
        icon: ModerationIcon,
        count: null,
        show: false,
      },
    ],
  }),
})
