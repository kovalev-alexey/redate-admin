import { API } from './apiService.js'
import { UserService } from '@/services/user'

class ApiService {
  async userInfo() {
    try {
      const userInfoResponse = await API.get('/admin/info')

      if (!userInfoResponse.status) {
        if (userInfoResponse.code === 401 || userInfoResponse.code === 403) {
          UserService.logout()

          return
        }

        throw new Error(userInfoResponse.message)
      }

      return {
        data: userInfoResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
}

export const UserApi = new ApiService()
