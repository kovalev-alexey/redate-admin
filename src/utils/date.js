const addLeadingZero = (num) =>
  num.toString().length === 1 ? `0${num}` : num.toString()

const getFullDate = (date) => {
  const day = addLeadingZero(date.getDate())
  const month = addLeadingZero(date.getMonth() + 1)
  const year = date.getFullYear()
  const hours = addLeadingZero(date.getHours())
  const minutes = addLeadingZero(date.getMinutes())

  return `${day}.${month}.${year}, ${hours}:${minutes}`
}

export { getFullDate }
