const routes = [
  {
    path: '/auth',
    name: 'Auth',
    components: {
      default: () => import('@/views/Auth'),
    },
    meta: {
      layout: 'auth',
      auth: false,
      hideSidebar: true,
    },
  },
  {
    path: '/',
    name: 'Complaints',
    components: {
      default: () => import('@/views/Complaints'),
    },
    meta: {
      layout: 'default',
      auth: true,
      hideSidebar: false,
    },
  },
  {
    path: '/mailings',
    name: 'Mailings',
    components: {
      default: () => import('@/views/Mailings'),
    },
    meta: {
      layout: 'mailings',
      auth: true,
      hideSidebar: false,
    },
  },
  {
    path: '/verification',
    name: 'Verification',
    components: {
      default: () => import('@/views/Verification'),
    },
    meta: {
      layout: 'verification',
      auth: true,
      hideSidebar: false,
    },
  },
  {
    path: '/moderation',
    name: 'Moderation',
    components: {
      default: () => import('@/views/Moderation'),
    },
    meta: {
      layout: 'verification',
      auth: true,
      hideSidebar: false,
    },
  },
  {
    path: '/support-chat',
    name: 'SupportChat',
    components: {
      default: () => import('@/views/SupportChat'),
    },
    meta: {
      layout: 'default',
      auth: true,
      hideSidebar: false,
    },
  },
  {
    path: '/users-chat',
    name: 'UsersChat',
    components: {
      default: () => import('@/views/UsersChat'),
    },
    meta: {
      layout: 'default',
      auth: true,
      hideSidebar: false,
    },
  },
  {
    path: '/blocking',
    name: 'Blocking',
    components: {
      default: () => import('@/views/Blocking'),
    },
    meta: {
      layout: 'default',
      auth: true,
      hideSidebar: false,
    },
  },
  {
    path: '/search/:id',
    name: 'SearchUser',
    components: {
      default: () => import('@/views/Search/SearchUser.vue'),
    },
    meta: {
      layout: 'search',
      auth: true,
    },
  },
  {
    path: '/search',
    name: 'Search',
    components: {
      default: () => import('@/views/Search'),
    },
    meta: {
      layout: 'search',
      auth: true,
      hideSidebar: false,
    },
  },
  {
    path: '/user-chats',
    name: 'ChatsPage',
    components: {
      default: () => import('@/views/ChatsPage'),
    },
    meta: {
      layout: 'default',
      auth: true,
      hideSidebar: false,
    },
  },
  {
    path: '/chat/:user',
    name: 'SingleChat',
    components: {
      default: () => import('@/views/UsersChat'),
    },
    meta: {
      layout: 'default',
      auth: true,
      hideSidebar: false,
    },
  },
  {
    path: '/metrics',
    name: 'Metrics',
    components: {
      default: () => import('@/views/Metrics'),
    },
    meta: {
      layout: 'default',
      auth: true,
      hideSidebar: false,
    },
  },
  {
    path: '/admin-control',
    name: 'AdminControl',
    components: {
      default: () => import('@/views/AdminControl'),
    },
    meta: {
      layout: 'default',
      auth: true,
      hideSidebar: false,
    },
  },
  {
    path: '/403',
    name: 'AccessDenied',
    components: {
      default: () => import('@/views/AccessDenied'),
    },
    meta: {
      layout: 'auth',
      auth: true,
      hideSidebar: true,
    },
  },
  {
    path: '/500',
    name: 'ServerError',
    components: {
      default: () => import('@/views/ServerError'),
    },
    meta: {
      layout: 'auth',
      auth: true,
      hideSidebar: true,
    },
  },
  {
    path: '/:notFound(.*)',
    name: 'NotFound',
    components: {
      default: () => import('@/views/NotFound'),
    },
    meta: {
      layout: 'auth',
      auth: true,
    },
  },
]

export { routes }
