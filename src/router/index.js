import { createRouter, createWebHistory } from 'vue-router'
import { routes } from './routes'
import { AuthService } from '@/views/Auth/services'
import { useUserStore } from '@/stores/user'

const router = createRouter({
  history: createWebHistory(),
  routes,
  scrollBehavior() {
    return {
      top: 0,
    }
  },
})

router.beforeEach(async (to) => {
  const userStore = useUserStore()

  const tokens = userStore.tokens.data || null

  if (tokens === null && to.meta.auth) {
    if (to.name === 'Auth') {
      return
    }

    return { name: 'Auth' }
  }

  if (tokens !== null) {
    const date = Date.now()

    const { expiredAt } = tokens.access

    if (date > expiredAt) {
      const { expiredAt } = tokens.refresh

      if (date > expiredAt) {
        return { name: 'Auth' }
      }

      const updatedTokens = await AuthService.updateTokens()

      if (updatedTokens.status) {
        return { name: 'Auth' }
      }

      if (!updatedTokens.access?.token) {
        return { name: 'Auth' }
      }
    }

    if (!to.meta.auth) {
      return { name: 'Complaints' }
    }
  }
})

export default router
